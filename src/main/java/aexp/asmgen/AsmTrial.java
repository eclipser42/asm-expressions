package aexp.asmgen;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.ACC_FINAL;
import static org.objectweb.asm.Opcodes.ACC_INTERFACE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.V1_5;
import static org.objectweb.asm.Opcodes.V1_7;

import java.lang.reflect.InvocationTargetException;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;

public class AsmTrial {
    static class MyClassLoader extends ClassLoader {
        public Class defineClass(String name, byte[] b) {
            return defineClass(name, b, 0, b.length);
        }
    }

    public void something() {
        ClassWriter cwM = new ClassWriter(COMPUTE_MAXS);
        cwM.visit(V1_5, ACC_INTERFACE, "pkg/Mesurable", null, "java/lang/Object", null);
        cwM.visitEnd();
        MyClassLoader loader = new MyClassLoader();
        loader.defineClass("pkg.Mesurable", cwM.toByteArray());

        ClassWriter cw = new ClassWriter(0);
        cw.visit(V1_5, ACC_PUBLIC + ACC_ABSTRACT + ACC_INTERFACE, "pkg/Comparable", null, "java/lang/Object",
                new String[] { "pkg/Mesurable" });
        cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "LESS", "I", null, Integer.valueOf(-1)).visitEnd();
        cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "EQUAL", "I", null, Integer.valueOf(0)).visitEnd();
        cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "GREATER", "I", null, Integer.valueOf(1)).visitEnd();
        cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "compareTo", "(Ljava/lang/Object;)I", null, null).visitEnd();
        cw.visitEnd();
        loader.defineClass("pkg.Comparable", cw.toByteArray());
    }

    public static Object zero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
        ClassWriter cw = new ClassWriter(COMPUTE_MAXS);
        cw.visit(V1_7, ACC_PUBLIC, "pkg/Zero", null, "java/lang/Object", null);
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "zero", "()D", null, null);
        mv.visitCode();
        mv.visitLdcInsn(4d);
        mv.visitInsn(DRETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();
        cw.visitEnd();
        MyClassLoader loader = new MyClassLoader();
        Class zero = loader.defineClass("pkg.Zero", cw.toByteArray());
        return zero.getDeclaredMethods()[0].invoke(null);
    }
}
