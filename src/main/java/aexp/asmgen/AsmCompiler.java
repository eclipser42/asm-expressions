package aexp.asmgen;

import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;
import static org.objectweb.asm.Opcodes.*;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import aexp.model.Expression;
import aexp.model.Expressions.Add;
import aexp.model.Expressions.Constant;
import aexp.model.Expressions.Divide;
import aexp.model.Expressions.Lookup;
import aexp.model.Expressions.Multiply;
import aexp.model.Expressions.Subtract;

/**
 * {@code AsmEvaluator} that currently supports:
 * <ul>
 * <li>{@link Add}</li>
 * <li>{@link Constant}</li>
 * <li>{@link Lookup}</li>
 * </ul>
 */
public class AsmCompiler implements AsmEvaluator {

    /** Package of the generated class. */
    private static final String PACKAGE = "gen";

    @Override
    public Expression compile(Expression expr) {
        final String className = "Add";
        final byte[] code = generate(className, expr);
        return buildInterface(className, code);
    }

    private byte[] generate(String className, Expression expr) {
        final String internalName = PACKAGE + "/" + className;
        ClassWriter cw = new ClassWriter(COMPUTE_MAXS);
        cw.visit(V1_7, ACC_PUBLIC, internalName, null, "java/lang/Object", new String[] { "aexp/model/Expression" });
        MethodVisitor ctor = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        ctor.visitCode();
        ctor.visitVarInsn(ALOAD, 0);
        ctor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        ctor.visitInsn(RETURN);
        ctor.visitMaxs(0, 0);
        ctor.visitEnd();
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "evaluate", "([D)D", null, null);
        mv.visitCode();

        generateInstructionsForTree(mv, expr);

        mv.visitInsn(DRETURN);
        mv.visitMaxs(0, 0);
        // TODO: mv.visitFrame() for branches
        mv.visitEnd();
        cw.visitEnd();
        return cw.toByteArray();
    }

    private void generateInstructionsForTree(MethodVisitor mv, Expression expr) {
        if (expr instanceof Add) {
            generateInstructionsForAdd(mv, (Add) expr);
        } else if (expr instanceof Divide) {
            generateInstructionsForDivide(mv, (Divide) expr);
        } else if (expr instanceof Multiply) {
            generateInstructionsForMultiply(mv, (Multiply) expr);
        } else if (expr instanceof Subtract) {
            generateInstructionsForSubtract(mv, (Subtract) expr);
        } else if (expr instanceof Constant) {
            generateInstructionsForConstant(mv, (Constant) expr);
        } else if (expr instanceof Lookup) {
            generateInstructionsForLookup(mv, (Lookup) expr);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private void generateInstructionsForAdd(MethodVisitor mv, Add expr) {
        generateInstructionsForTree(mv, expr.leftOperand);
        generateInstructionsForTree(mv, expr.rightOperand);
        mv.visitInsn(DADD);
    }

    private void generateInstructionsForDivide(MethodVisitor mv, Divide expr) {
        generateInstructionsForTree(mv, expr.leftOperand);
        generateInstructionsForTree(mv, expr.rightOperand);
        mv.visitInsn(DDIV);
    }

    private void generateInstructionsForMultiply(MethodVisitor mv, Multiply expr) {
        generateInstructionsForTree(mv, expr.leftOperand);
        generateInstructionsForTree(mv, expr.rightOperand);
        mv.visitInsn(DMUL);
    }
    
    private void generateInstructionsForSubtract(MethodVisitor mv, Subtract expr) {
        generateInstructionsForTree(mv, expr.leftOperand);
        generateInstructionsForTree(mv, expr.rightOperand);
        mv.visitInsn(DSUB);
    }
    
    private void generateInstructionsForConstant(MethodVisitor mv, Constant expr) {
        mv.visitLdcInsn(expr.value);
    }

    private void generateInstructionsForLookup(MethodVisitor mv, Lookup expr) {
        mv.visitVarInsn(ALOAD, 1);
        mv.visitLdcInsn(expr.index);
        mv.visitInsn(DALOAD);
    }
    
    private static class ExpressionClassLoader extends ClassLoader {
        @SuppressWarnings("unchecked")
        public Class<Expression> defineClass(String name, byte[] b) {
            return (Class<Expression>) defineClass(name, b, 0, b.length);
        }
    }

    private Expression buildInterface(String className, byte[] code) {
        final String longName = PACKAGE + "." + className;
        final Class<Expression> genClass = new ExpressionClassLoader().defineClass(longName, code);
        try {
            return (Expression) genClass.getConstructor().newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

}
