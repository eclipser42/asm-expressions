package aexp.model;

public class Expressions {

    public static class Add extends BinaryExpression {
        public Add(Expression left, Expression right) {
            this.leftOperand = left;
            this.rightOperand = right;
        }

        public double evaluate(double[] features) {
            return leftOperand.evaluate(features) + rightOperand.evaluate(features);
        }
    }

    public static class Divide extends BinaryExpression {
        public Divide(Expression left, Expression right) {
            this.leftOperand = left;
            this.rightOperand = right;
        }

        public double evaluate(double[] features) {
            return leftOperand.evaluate(features) / rightOperand.evaluate(features);
        }
    }

    public static class Multiply extends BinaryExpression {
        public Multiply(Expression left, Expression right) {
            this.leftOperand = left;
            this.rightOperand = right;
        }

        public double evaluate(double[] features) {
            return leftOperand.evaluate(features) * rightOperand.evaluate(features);
        }
    }

    public static class Subtract extends BinaryExpression {
        public Subtract(Expression left, Expression right) {
            this.leftOperand = left;
            this.rightOperand = right;
        }

        public double evaluate(double[] features) {
            return leftOperand.evaluate(features) - rightOperand.evaluate(features);
        }
    }
    
    public static class Constant implements Expression {
        public double value;
        
        public Constant(double value) {
            this.value = value;
        }
        
        public double evaluate(double[] features) {
            return value;
        }
    }

    public static class Lookup implements Expression {
        public int index;

        public Lookup(int index) {
            this.index = index;
        }

        @Override
        public double evaluate(double[] features) {
            return features[index];
        }
    }
}
