package aexp.model;

public abstract class BinaryExpression implements Expression {
    public Expression leftOperand;
    public Expression rightOperand;
}
