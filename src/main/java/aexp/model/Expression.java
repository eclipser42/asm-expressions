package aexp.model;

import aexp.model.Expressions.Add;
import aexp.model.Expressions.Divide;
import aexp.model.Expressions.Multiply;
import aexp.model.Expressions.Subtract;

public interface Expression {
    double evaluate(double[] features);

    default Divide divideBy(Expression value) {
        return new Expressions.Divide(this, value);
    }

    default Subtract minus(Expression value) {
        return new Expressions.Subtract(this, value);
    }

    default Multiply multiplyBy(Expression value) {
        return new Expressions.Multiply(this, value);
    }

    default Add plus(Expression value) {
        return new Expressions.Add(this, value);
    }
}
