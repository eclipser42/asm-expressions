package aexp.model;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import aexp.model.Expressions.*;


public class ExpressionsTest {

    public static Expression ZERO = new Constant(0);
    public static Expression ONE = new Constant(1);
    public static Expression TWO = new Constant(2);
    public static double[] features = {1.9, 1.7, 1.3, 1.1, 0.7, 0.3, 0.2};
    public static Expression THREE = new Add(new Lookup(0), new Lookup(3));

    @Test
    public void testConstants() {
        assertThat(ZERO.evaluate(null)).isEqualTo(0);
        assertThat(ONE.evaluate(null)).isEqualTo(1);
        assertThat(TWO.evaluate(null)).isEqualTo(2);

        Expression add = new Add(ONE, TWO);
        assertEvaluation(add, 3);

        Expression subtract = new Subtract(ONE, TWO);
        assertEvaluation(subtract, -1);

        Expression multiply = new Multiply(TWO, ONE);
        assertEvaluation(multiply, 2);

        Expression divide = new Divide(TWO, ONE);
        assertEvaluation(divide, 2);
    }

    @Test
    public void testSingle() {
        // choose values so double representations are precise
        Expression three = new Lookup(5);
        Expression seven = new Lookup(4);

        Expression add = new Add(three, seven);
        assertEvaluation(add, 1);

        Expression subtract = new Subtract(seven, new Lookup(1));
        assertEvaluation(subtract, -1);

        Expression multiply = new Multiply(three, TWO);
        assertEvaluation(multiply, 0.6);

        Expression divide = new Divide(THREE, TWO);
        assertEvaluation(divide, 1.5);
    }

    private void assertEvaluation(Expression subject, final double expected) {
        baseAssertion(subject, expected);
        assertIdentities(subject);
    }

    private void assertIdentities(Expression subject) {
        final double baseValue = subject.evaluate(features);
        baseAssertion(subject.plus(ZERO), baseValue);
        baseAssertion(subject.minus(ZERO), baseValue);
        baseAssertion(subject.multiplyBy(ONE), baseValue);
        baseAssertion(subject.divideBy(ONE), baseValue);
    }

    protected void baseAssertion(Expression subject, final double expected) {
        assertThat(subject.evaluate(features)).isEqualTo(expected);
    }
}
