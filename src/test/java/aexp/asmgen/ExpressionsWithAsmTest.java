package aexp.asmgen;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import aexp.model.Expression;
import aexp.model.ExpressionsTest;

public class ExpressionsWithAsmTest extends ExpressionsTest {
    AsmEvaluator compiler = new AsmCompiler();

    @Override
    protected void baseAssertion(final Expression subject, final double expected) {
        assertThat(compiler.compile(subject).evaluate(features)).isEqualTo(expected);
    }

    @Test
    public void testSingle() {
        super.testSingle();
    }
}