package aexp.asmgen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AsmTrialTest {

    @Test
    void testSomething() {
        new AsmTrial().something();
    }

    @Test
    void testFour() throws Exception {
        assertEquals(Double.valueOf(4), AsmTrial.zero());
    }
}
