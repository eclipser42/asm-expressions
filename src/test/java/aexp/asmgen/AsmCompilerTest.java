package aexp.asmgen;

import static aexp.model.ExpressionsTest.ONE;
import static aexp.model.ExpressionsTest.TWO;
import static aexp.model.ExpressionsTest.THREE;
import static aexp.model.ExpressionsTest.features;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import aexp.model.Expression;
import aexp.model.Expressions.Add;

public class AsmCompilerTest {

    @Test
    public void testCompile() {
        AsmEvaluator compiler = new AsmCompiler();
        Add add = ONE.plus(TWO);
        Expression subject = compiler.compile(add);
        assertThat(subject.evaluate(null)).isEqualTo(3);
    }

    @Test
    public void testRecursiveCompile() {
        AsmEvaluator compiler = new AsmCompiler();
        Add compound = TWO.plus(ONE.plus(TWO));
        Expression subject = compiler.compile(compound);
        assertThat(subject.evaluate(null)).isEqualTo(5);
    }

    @Test
    public void testCompileWithLookup() {
        AsmEvaluator compiler = new AsmCompiler();
        Add add = THREE.plus(TWO);
        Expression subject = compiler.compile(add);
        assertThat(subject.evaluate(features)).isEqualTo(5);
    }
}
